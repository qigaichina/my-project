package workflow

import (
	"fmt"
	cc_redis "my-project/reids"
	"strconv"
	"strings"
)

type flowTaskData struct {
	taskIdList     []string    //任务切片
	taskList       []*workTask `json:"-"`
	currentTaskId  string
	currentTaskTag string
	result         bool //任务流执行结果
}

type workFlow struct {
	flowTaskData
	id         string //任务流ID，唯一
	flowTag    string
	repeatMode RepeatExecuteMode
	size       int
}

type RepeatExecuteMode int

const (
	FROM_FIRST_TASK RepeatExecuteMode = iota
	LAST_UNEXEC_TASK
)

type WorkFlowOption func(d *workFlow)

func NewWorkFlow(flowId, flowTag string, opts ...WorkFlowOption) *workFlow {
	w := &workFlow{
		id:         flowId,
		flowTag:    flowTag,
		repeatMode: FROM_FIRST_TASK,
	}

	for _, f := range opts {
		f(w)
	}

	return w
}

func WithRepeatExecuteMode(repeatMode RepeatExecuteMode) WorkFlowOption {
	return func(r *workFlow) {
		r.repeatMode = repeatMode
	}
}

var WORKFLOW_REDIS_KEY = "workflow_key"

func (w *workFlow) AppendTask(task *workTask) *workFlow {

	if len(task.taskId) == 0 {
		task.taskId = w.id + "_task_" + strconv.Itoa(len(w.taskIdList))
	}

	fmt.Println("task.TaskId : ", task.taskId)
	if w.size == 0 {
		task.next = nil
		task.prev = nil
	} else {
		task.prev = w.taskList[w.size-1]
		task.next = nil
		w.taskList[w.size-1].next = task
	}

	w.taskIdList = append(w.taskIdList, task.taskId)
	w.taskList = append(w.taskList, task)

	w.size++

	return w
}

func (w *workFlow) Execute() {

	if w.size == 0 {
		return
	}

	fmt.Println("WorkFlow Execute id =", w.id, "Size =", len(w.taskIdList), "TaskIdList =", w.taskIdList)

	var isRepeat = false

	//判断是否重复任务流
	var workFlow workFlow
	cc_redis.RedisGetObject(WORKFLOW_REDIS_KEY+":"+w.id, &workFlow)
	if len(workFlow.id) != 0 {
		isRepeat = true
		fmt.Println("repeat workflow :", workFlow)
	}

	for _, task := range w.taskList {
		if w.repeatMode == LAST_UNEXEC_TASK {
			if strings.Compare(w.currentTaskTag, task.taskTag) != 0 {
				continue
			}
		}

		if err := task.execute(isRepeat); err != nil {
			rollBackTask := task.prev
			for rollBackTask != nil {
				rollBackTask.Rollback()
				rollBackTask = rollBackTask.prev
			}
			return
		}
		w.currentTaskTag = task.taskTag
		cc_redis.RedisSetObject(WORKFLOW_REDIS_KEY+":"+w.id, w)
	}

	w.result = true
	fmt.Println("WorkFlow id:", w.id, "is completed")
	cc_redis.RedisSetObject(WORKFLOW_REDIS_KEY+":"+w.id, w)
}
