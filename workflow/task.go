package workflow

import (
	"fmt"
	cc_redis "my-project/reids"
)

var TASK_REDIS_KEY = "task_redis_key"

type workTaskData struct {
	context string    //任务执行上下文
	result  bool      //任务执行结果
	prev    *workTask `json:"-"` //前一个任务
	next    *workTask `json:"-"` //后一个任务
}

type TaskDoFunc func(isRepeat bool, oldContext string) (context string, err error)
type TaskUnDoFunc func(data string)

type workTask struct {
	workTaskData
	taskId  string //任务ID
	taskTag string
	do      TaskDoFunc   `json:"-"` //任务do方法
	unDo    TaskUnDoFunc `json:"-"` //任务回滚方法
}

func NewWorkTask(taskTag string) *workTask {
	return &workTask{
		taskTag: taskTag,
	}
}

func (t *workTask) SetDoFunc(do TaskDoFunc) *workTask {
	t.do = do
	return t
}

func (t *workTask) SetUnDoFunc(unDo TaskUnDoFunc) *workTask {
	t.unDo = unDo
	return t
}

func (t *workTask) execute(isRepeat bool) error {
	fmt.Println("Execute task :", t.taskId, "isRepeat :", isRepeat)
	var task workTask
	if isRepeat {
		cc_redis.RedisGetObject(TASK_REDIS_KEY+":"+t.taskId, &task)
	}

	if t.do == nil {
		return nil
	}

	data, err := t.do(task.result, task.context)
	t.context = data
	if err == nil {
		t.result = true
		cc_redis.RedisSetObject(TASK_REDIS_KEY+":"+t.taskId, t)
	}
	return err
}

func (t *workTask) Rollback() {
	if len(t.context) == 0 {
		t.context, _ = cc_redis.RedisGetStringVal(TASK_REDIS_KEY + ":" + t.taskId)
	}

	if t.unDo == nil {
		return
	}

	fmt.Println("Rollback task : ", t.taskId, " Value : ", t.context)

	t.unDo(t.context)
}
