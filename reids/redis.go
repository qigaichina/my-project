package cc_redis

import (
	"encoding/json"
	"fmt"
	"time"

	redis "github.com/go-redis/redis"
)

var redisClient *redis.Client

var reidsHost = "127.0.0.1"
var port = "6379"
var password = ""

// SetRedis 设置redis
func InitRedisClient() {
	fmt.Println("-------启动redis--------")
	redisClient = redis.NewClient(&redis.Options{
		Addr:     reidsHost + ":" + port,
		Password: "",
	})
	pong, err := redisClient.Ping().Result()
	if err != nil {
		fmt.Printf("------------连接 redis：%s 失败,原因：%v\n", reidsHost+":"+port, err)
		return
	}
	fmt.Printf("---------连接 redis  成功, %v\n", pong)
}

// RedisSetObject 将值保存到redis
func RedisSetObject(key string, value interface{}) error {
	return RedisSetObjectSpecifyTimeOut(key, value, time.Hour*1)
}

// RedisSetObject 将值保存到redis，并指定超时时间
func RedisSetObjectSpecifyTimeOut(key string, value interface{}, expiration time.Duration) error {
	data, _ := json.Marshal(value)
	return redisClient.Set(key, data, expiration).Err()
}

// RedisGetObject 从redis获取值
func RedisGetObject(key string, resultValue interface{}) error {
	value, err := redisClient.Get(key).Result()
	if err != nil {
		return err
	}
	return json.Unmarshal([]byte(value), &resultValue)
}

// RedisSetVal 将值保存到redis
func RedisSetStringVal(key string, value string) error {
	fmt.Println("RedisSetStringVal key", key, " value : ", value)
	return redisClient.Set(key, value, time.Hour*1).Err()
}

// RedisGetVal 从redis获取值
func RedisGetStringVal(key string) (string, error) {
	fmt.Println("RedisGetStringVal key", key)
	return redisClient.Get(key).Result()
}

func RedisDelKey(key string) error {
	fmt.Println("RedisDelKey ", key)
	_, err := redisClient.Del(key).Result()
	return err
}
